package com.app.golokal.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.app.golokal.R;
import com.app.golokal.Volley.ApiCall;
import com.app.golokal.Volley.VolleyCallback;
import com.app.golokal.adapters.EditAddressAdapter;
import com.app.golokal.helpers.UrlHelper;
import com.app.golokal.helpers.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class AddressActivity extends AppCompatActivity {

    public static String TAG = AddressActivity.class.getSimpleName();
    public static JSONArray addressArray = new JSONArray();
    public  static RecyclerView address_list;
    ImageView addButton, backButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.BLACK);
        }
        initViews();
        SetAddressAdapter(AddressActivity.this);
        initListners();
    }

    private void initListners() {
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AddressActivity.this, MapActivity.class);
                intent.putExtra("from", "save");
                startActivity(intent);
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void initViews() {

        address_list = (RecyclerView) findViewById(R.id.address_list);
        addButton = (ImageView) findViewById(R.id.addButton);
        backButton = (ImageView) findViewById(R.id.backButton);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onResume() {
        super.onResume();
        SetAddressAdapter(AddressActivity.this);
    }

    public static void SetAddressAdapter(final Context context) {

        ApiCall.getMethodHeaders(context, UrlHelper.LIST_ADDRESS, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                Utils.log(TAG, "response:" + response);
                addressArray = response.optJSONArray("list_address");


                LinearLayoutManager addressLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
                address_list.setLayoutManager(addressLayoutManager);
                EditAddressAdapter addressAdapter = new EditAddressAdapter(context, addressArray);
                address_list.setAdapter(addressAdapter);
            }
        });


    }


    public  static void deleteId(final Context context, String id) throws JSONException {
        JSONObject jsonObject=new JSONObject();
        jsonObject.put("id",id);
        ApiCall.PostMethodHeaders(context, UrlHelper.DELETE_ADDRESS, jsonObject,new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                Utils.log(TAG, "response:" + response);
             SetAddressAdapter(context);
            }
        });
    }
}
