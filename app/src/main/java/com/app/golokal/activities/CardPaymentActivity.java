package com.app.golokal.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.app.golokal.R;
import com.app.golokal.Volley.ApiCall;
import com.app.golokal.Volley.VolleyCallback;
import com.app.golokal.helpers.UrlHelper;
import com.app.golokal.helpers.Utils;

import org.json.JSONException;
import org.json.JSONObject;

public class CardPaymentActivity extends AppCompatActivity {
    private ProgressBar progressBar;
    private String bookingId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_payment);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.BLACK);
        }
        ImageView backButton = findViewById(R.id.backButton);
        progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.VISIBLE);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CardPaymentActivity.this.finish();
            }
        });
        Intent intent = getIntent();
        bookingId = intent.getStringExtra("bookingId");
        String bookingOrderId = intent.getStringExtra("bookingOrderId");
        // Log.e("ff", "onCreate: " + bookingOrderId);
        movePaymentActivity(bookingOrderId);
    }

    private void movePaymentActivity(String id) {
        WebView webView = findViewById(R.id.webView);
        webView.loadUrl("http://178.62.111.58/uber_test/public/api/create_payment?order_id=" + id);
        // Log.e("url", "card: " + "http://178.62.111.58/uber_test/public/api/create_payment?orderid=" + id);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new YXWebViewClient());
    }

    public class YXWebViewClient extends WebViewClient {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            //  Log.d("checkValue", "onPageStarted: " + url);
            if (url.equalsIgnoreCase("http://google.com/")) {
                hitPaymentMethod(bookingId);
            }
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            progressBar.setVisibility(View.GONE);
            super.onPageFinished(view, url);
            // Log.i("Listener", "onPageStarted" + url);
        }
    }

    private void hitPaymentMethod(String booking_id) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("booking_id", booking_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiCall.PostMethodHeaders(CardPaymentActivity.this, UrlHelper.CHARGE, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                // Log.e("cardPay", "onSuccess: " + response);
                Toast.makeText(CardPaymentActivity.this, R.string.payment_completed, Toast.LENGTH_LONG).show();
                CardPaymentActivity.this.finish();
            }
        });
    }
}