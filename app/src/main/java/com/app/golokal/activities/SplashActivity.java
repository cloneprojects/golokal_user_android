package com.app.golokal.activities;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.app.golokal.R;
import com.app.golokal.Volley.ApiCall;
import com.app.golokal.Volley.VolleyCallback;
import com.app.golokal.helpers.UrlHelper;
import com.app.golokal.helpers.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SplashActivity extends AppCompatActivity {
    AppSettings appSettings = new AppSettings(SplashActivity.this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    getPackageName(),
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }

//        ImageView spinningEarth = (ImageView)findViewById(R.id.bottomWorld);
//        RotateAnimation rotateAnimation = new RotateAnimation(360,0, Animation.RELATIVE_TO_SELF,.5f,Animation.RELATIVE_TO_SELF,.5f);
//        rotateAnimation .setDuration(50000);
//        rotateAnimation .setFillAfter(true);
//        rotateAnimation .setRepeatCount(-1);
//        CycleInterpolator interpolator= new CycleInterpolator(2);
//        rotateAnimation.setInterpolator(new CycleInterpolator(2));
//
//        spinningEarth.setAnimation(rotateAnimation);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                if (appSettings.getIsLogged().equalsIgnoreCase("true")) {
                    upDateToken();

                    moveMainActivity();

                } else {
                    moveOnBoardActivity();

                }
            }
        }, 3000);
       // movePaymentActivity();
    }

   /* private void movePaymentActivity() {

//        Intent onboard = new Intent(SplashActivity.this,DibsPaymentLibraryExampleApp.class);
//        startActivity(onboard);
//        finish();

        WebView webView = findViewById(R.id.webView);
        webView.loadUrl("http://178.62.111.58/uber_test/public/api/create_payment");
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new YXWebViewClient());

    }*/

    private void upDateToken() {
        ApiCall.PostMethodHeaders(SplashActivity.this, UrlHelper.UPDATE_DEVICE_TOKEN, getInputs(), new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
            }
        });
    }

   /* public class YXWebViewClient extends WebViewClient {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            Log.d("checkValue", "onPageStarted: " + url);
            if (url.equalsIgnoreCase("http://google.com/")) {
                Utils.toast(SplashActivity.this, getResources().getString(R.string.payment_completed));
            }

        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            Log.i("Listener", "onPageStarted" + url);
        }

    }*/

    private JSONObject getInputs() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("fcm_token", appSettings.getFireBaseToken());
            jsonObject.put("os", "android");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }

    private void moveOnBoardActivity() {
        Intent onboard = new Intent(SplashActivity.this, OnboardActivity.class);
        startActivity(onboard);
        finish();
    }

    private void moveMainActivity() {
        Intent intent = new Intent(SplashActivity.this, MainActivity.class);
        intent.putExtra("type", "new");
        startActivity(intent);
        finish();

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
