package com.app.golokal.activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.golokal.R;
import com.app.golokal.helpers.Utils;
import com.bumptech.glide.Glide;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class TestingActivity extends AppCompatActivity {
    CardView addPhoto;
    ImageView imageOne, imageTwo, imageThree;
    TextView countText;
    List<String> totalImages = new ArrayList<>();
    private Uri uri;
    private File uploadFile;
    View overlayImage;

    public static String getRealPathFromURI(Context context, Uri contentURI) {
        String result;
        Cursor cursor = context.getContentResolver().query(contentURI, null,
                null, null, null);

        if (cursor == null) {
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor
                    .getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_testing);
        imageOne = findViewById(R.id.imageOne);
        imageTwo = findViewById(R.id.imageTwo);
        imageThree = findViewById(R.id.imageThree);
        addPhoto = findViewById(R.id.addPhoto);
        countText = findViewById(R.id.countText);
        overlayImage = findViewById(R.id.overlayImage);
        addPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPictureDialog();
            }
        });

    }

    private void showPictureDialog() {

        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle(getResources().getString(R.string.choose_your_option));
        String[] items = {getResources().getString(R.string.gallery), getResources().getString(R.string.camera)};

        dialog.setItems(items, new DialogInterface.OnClickListener() {

            @TargetApi(Build.VERSION_CODES.M)
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                switch (which) {
                    case 0:
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 101);
                        } else {
                            choosePhotoFromGallary();
                        }
                        break;
                    case 1:

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(new String[]{Manifest.permission.CAMERA}, 100);
                        } else {
                            takePhotoFromCamera();
                        }


                        break;

                }
            }
        });
        dialog.show();
    }

    private void choosePhotoFromGallary() {
        Intent i = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, 1);

    }

    private void takePhotoFromCamera() {

        Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(i, 2);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.e("requestCode", "" + requestCode);
        switch (requestCode) {
            case 100:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    takePhotoFromCamera();
                } else {
                    Utils.toast(TestingActivity.this, getResources().getString(R.string.camera_permission_error));

                }
                break;

            case 101:

                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    choosePhotoFromGallary();
                } else {
                    Utils.toast(TestingActivity.this, getResources().getString(R.string.storage_permission_error));

                }

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case 1://gallery
                if (data != null) {

                    uri = data.getData();
                    if (uri != null) {
                        handleimage(uri);
                    } else {
                        Utils.toast(TestingActivity.this, getResources().getString(R.string.unable_to_select_image));
                    }
                }
                break;
            case 2://camera
                if (data != null) {
                    Bundle extras = data.getExtras();
                    Bitmap imageBitmap = (Bitmap) extras.get("data");

                    Uri tempUri = getImageUri(getApplicationContext(), imageBitmap);
                    uploadFile = new File(getRealPathFromURI(TestingActivity.this, tempUri));
                    totalImages.add(uploadFile.getAbsolutePath());
                    loadImages();

                }
                break;


        }
    }

    public void loadImages() {

        for (int i = 0; i < totalImages.size(); i++) {
            if (i == 0) {
                Glide.with(TestingActivity.this).load(totalImages.get(0)).into(imageOne);
            } else if (i == 1) {
                Glide.with(TestingActivity.this).load(totalImages.get(1)).into(imageTwo);

            }else if (i == 2) {
                Glide.with(TestingActivity.this).load(totalImages.get(2)).into(imageThree);

            }
        }

        if (totalImages.size()>2)
        {
            overlayImage.setVisibility(View.VISIBLE);
            countText.setVisibility(View.VISIBLE);
            countText.setText("+"+(totalImages.size() - 3));

        } else {
            overlayImage.setVisibility(View.GONE);
            countText.setVisibility(View.GONE);
        }

    }

    public static Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "ProfilePic", "UberX");
        return Uri.parse(path);
    }



    private void handleimage(Uri uri) {


        uploadFile = new File(getRealPathFromURI(TestingActivity.this, uri));
        totalImages.add(uploadFile.getAbsolutePath());
        loadImages();

    }


}
