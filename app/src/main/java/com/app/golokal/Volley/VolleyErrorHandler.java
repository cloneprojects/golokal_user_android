package com.app.golokal.Volley;

import android.content.Context;
import android.content.Intent;

import com.android.volley.VolleyError;

import com.app.golokal.R;
import com.app.golokal.activities.AppSettings;
import com.app.golokal.activities.SignInActivity;
import com.app.golokal.application.Uberdoo;
import com.app.golokal.helpers.Utils;

/**
 * Created by user on 19-09-2017.
 */

public class VolleyErrorHandler {


    private static String TAG=VolleyErrorHandler.class.getSimpleName();

    public static void handle(String url, VolleyError volleyError)
    {
        Context context= Uberdoo.getContext();

        try {
            Utils.log(TAG, "handle:url "+volleyError.networkResponse.statusCode);

            switch (volleyError.networkResponse.statusCode) {
                case 500:
                    Utils.toast(context, context.getResources().getString(R.string.some_thing_went_wrong));
                case 401:
                    moveToSignActivity(context);
                    Utils.toast(context,context.getResources().getString(R.string.please_login_again));
            }
        }
        catch (Exception e)
        {
            Utils.toast(context, context.getResources().getString(R.string.some_thing_went_wrong));

            e.printStackTrace();
        }
    }

    private static void moveToSignActivity(Context context) {
        Intent intent=new Intent(context, SignInActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        AppSettings appSettings=new AppSettings(context);
        appSettings.setIsLogged("false");
        context.startActivity(intent);

    }


}
