package com.app.golokal.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.app.golokal.R;
import com.app.golokal.activities.AppSettings;
import com.app.golokal.activities.SelectTimeAndAddressActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by karthik on 07/10/17.
 */
public class SubCategoriesAdapter extends RecyclerView.Adapter<SubCategoriesAdapter.MyViewHolder>{
    private Context context;
    private JSONArray subCategories;

    public SubCategoriesAdapter(Context context, JSONArray subCategories){
        this.context =context;
        this.subCategories = subCategories;
    }
     class MyViewHolder extends RecyclerView.ViewHolder {
        TextView subCategoryName;
        ImageView subCategoryImage;
        LinearLayout subCategoryLayout;

        MyViewHolder(View view) {
            super(view);
            subCategoryName =  view.findViewById(R.id.subCategoryName);
            subCategoryImage =  view.findViewById(R.id.subCategoryImage);
            subCategoryLayout = view.findViewById(R.id.subCategoryLayout);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_sub_category, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        try {
            final AppSettings appSettings=new AppSettings(context);

            final JSONObject subCategory = subCategories.getJSONObject(position);
            Log.d("onBindViewHolder: ","jsonObject"+subCategory);
            holder.subCategoryName.setText(subCategory.optString("sub_category_name"));

            Glide.with(context).load(subCategory.optString("icon")).into(holder.subCategoryImage);
            holder.subCategoryLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    appSettings.setSelectedSubCategory(subCategory.optString("id"));
                    appSettings.setSelectedSubCategoryName(subCategory.optString("sub_category_name"));
                    Intent timeAndAddress = new Intent(context, SelectTimeAndAddressActivity.class);
                    context.startActivity(timeAndAddress);

                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return subCategories.length();
    }
}
